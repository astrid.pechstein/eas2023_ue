#%%

import numpy as np
import matplotlib.pyplot as plt


data3d = np.loadtxt("frequenzlauf_3d.txt")
datapstrain = np.loadtxt("frequenzlauf_planestrain.txt")
datapstress = np.loadtxt("frequenzlauf_planestress.txt")

plt.semilogy(data3d[:,0], data3d[:,1], label="3d")
plt.semilogy(datapstrain[:,0], datapstrain[:,1], label="plane strain")
plt.semilogy(datapstress[:,0], datapstress[:,1], label="plane stress")

plt.xlabel(r"angular frequency $\omega$/rad s$^{-1}$")
plt.ylabel(r"tip deflection $u_z$/mm")
plt.legend()


plt.savefig("bimbeam_freq.png")
plt.show()

#%%