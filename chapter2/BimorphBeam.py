# %%
import netgen.gui
%gui tk
from netgen.csg import *
from ngsolve import *
SetHeapSize(100000000)
import numpy as np

# %% [markdown]
# ## Bimorph beam example, 3D
# 
# ### Geometry and Mesh
# use prismatic mesh of high order

# %%
#### Geometry
scale = 1e3  # --> computation in mm
h = 0.002*scale
b = 0.01*scale
l = 0.1*scale

## Plane means halfspace in CSG: 
## Plane(p, n) -- plane in Point p, outer normal vector n
## bc -- set boundary condition number
p_x0 =  Plane(Pnt(0,0,0),  Vec(-1,0,0)).bc("fix")
p_x1 = Plane(Pnt(l,0,0),  Vec(1,0,0)) .bc("tip")
p_z1 =   Plane(Pnt(0,0,h),  Vec(0,0,1)) .bc("free_el1")
p_z0 =  Plane(Pnt(0,0,-h), Vec(0,0,-1)).bc("free_el2")
p_zm = Plane(Pnt(0,0,0),  Vec(0,0,1)) .bc("free_el0")
p_y1 =  Plane(Pnt(0,b/2,0),  Vec(0,1,0)) .bc("free")
p_y0 = Plane(Pnt(0,-b/2,0),  Vec(0,-1,0)).bc("free")

## generate geometry from halfspaces
## consists of two hexaedra made from 6 planes
## * ... intersection 
## - ... difference
## + ... union
geometry = CSGeometry()
matnr_1 = geometry.Add( ((p_x0 * p_x1 * p_z1 * p_y1 * p_y0) - p_zm).mat("bot_mat"))
matnr_2 = geometry.Add( (p_x0 * p_x1* p_z0 * p_zm * p_y1 * p_y0).mat("top_mat"))
## define surfaces that are parallel, then prismatic elements are generated between these planes
geometry.CloseSurfaces(p_zm, p_z0, slices=[0.5])
geometry.CloseSurfaces(p_zm, p_z1, slices = [0.5])
geometry.Draw()
netgenmesh = geometry.GenerateMesh(maxh=0.005*scale)
ZRefinement(netgenmesh, geometry)
mesh = Mesh(netgenmesh)
Draw(mesh)


# %% [markdown]
# Two materials, 7 different boundary conditions

# %%
print("materials", mesh.GetMaterials())
print("boundaries", mesh.GetBoundaries())

# %% [markdown]
# ### Material constants etc

# %%
## useful - get strain vector representation

def strainvec(u):
    g = grad(u)
    return CoefficientFunction((g[0], g[4], g[8], g[5]+g[7], g[2]+g[6], g[1]+g[3]))

## material parameters
epsilon0 = 8.8541878e-12

CE_matrix = 1e9/scale/scale*np.array([[121, 75.9, 75.4, 0, 0, 0],
                          [75.9, 121, 75.4, 0, 0, 0],
                          [75.4, 75.4, 111, 0, 0, 0],
                          [0, 0, 0, 21.1, 0, 0],
                          [0, 0, 0, 0, 21.1, 0],
                          [0, 0, 0, 0, 0, 22.6]])

e15 = 12.3
e31 = -5.4
e33 = 15.8
e_matrix = 1/scale*np.array([[0, 0, 0, 0, e15, 0],
                     [0, 0, 0, e15, 0, 0],
                     [e31, e31, e33, 0, 0,0]])

epseps_matrix = epsilon0*np.array([[916, 0, 0],[0,916,0],[0,0,830]])


## material parameters as ngsolve coefficient functions

CEtensor = CoefficientFunction(tuple(CE_matrix.reshape(1, -1)[0]), dims=(6,6))
etensor = CoefficientFunction(tuple(e_matrix.reshape(1, -1)[0]), dims=(3,6))
epstensor = CoefficientFunction(tuple(epseps_matrix.reshape(1, -1)[0]), dims=(3,3))


# %% [markdown]
# ### FESpace, solution vector
# 

# %%
## the finite element solution space

## for the displacements - vector-valued H1 elements, dirichlet-zero condition on "fix" bd
V_u = VectorH1(mesh, order=4, dirichlet="fix")

## for the electric potential - H1 elements, dirichlet-zero condition on electrodes
V_phi = H1(mesh, order=4, dirichlet=".*el.")

## together 
V = V_u * V_phi
q = GridFunction(V)

u = q.components[0]
phi = q.components[1]

strain = strainvec(u)
elfield = -grad(phi)

stress = CEtensor*strain - etensor.trans*elfield
diel = etensor*strain + epstensor*elfield

Draw(u, mesh, "u")
Draw(phi, mesh, "phi")
Draw(BoundaryFromVolumeCF(elfield), mesh, "E")
Draw(BoundaryFromVolumeCF(strain), mesh, "strain")
Draw(BoundaryFromVolumeCF(diel), mesh, "D")
Draw(BoundaryFromVolumeCF(stress), mesh, "stress")



# %% [markdown]
# Initialize $\phi_0$

# %%
## potential Phi = Phi_0 + Phi_tilde (homogenzation of boundary conditions)
## set finite element function U_0 boundary values
phi_0 = GridFunction(V_phi)

phi_bdvalues = mesh.BoundaryCF({"free_el1": 75, "free_el2": 75, "free_el0": 0}, default=0)
phi_0.Set(phi_bdvalues, definedon=mesh.Boundaries(".*el."))

# %% [markdown]
# ### Bilinear form

# %%
## definition of the bilinear form 
## int sigma : deps - D dE dx
U, PHITILDE = V.TrialFunction()
DELTAU, DELTAPHI = V.TestFunction()

STRAIN = strainvec(U)
DELTASTRAIN = strainvec(DELTAU)

a = BilinearForm(V)
a += SymbolicBFI( InnerProduct(CEtensor*STRAIN + etensor.trans*(grad(PHITILDE)+grad(phi_0)), DELTASTRAIN) )
a += SymbolicBFI( InnerProduct(etensor*STRAIN - epstensor*(grad(PHITILDE)+grad(phi_0)), grad(DELTAPHI)) )


# %% [markdown]
# ### Solve the problem

# %%
## assemble stiffness matrix and load vector

q.vec[:] = 0
rhs = q.vec.CreateVector()

SetNumThreads(6)
with TaskManager():
    ## evaluate integrals for q=0, i.e. STRAIN=0 and PHITILDE=0 -> negative right hand side of equation
    a.Apply(q.vec, rhs)

    ## compute the stiffness matrix
    a.AssembleLinearization(q.vec)

    ## compute its inverse, for all those degrees of freedom that are not "dirichlet"
    ## use either "pardiso" (MKL sparse solver, usually available on windows/intel chips
    ## or "umfpack" (SuiteSparse solver, usually available on macos/non-intel chips
    inva = a.mat.Inverse(V.FreeDofs(), inverse="pardiso")  # inverse="umfpack"

    ## compute the solution (recall negative rhs)
    q.vec.data -= inva * rhs

    ## add phi_0 to electric potential
    phi.vec.data += phi_0.vec

# %% [markdown]
# ### Evaluate

# %%

## compute average tip deflection 
u_z = Integrate(u[2], mesh, definedon=mesh.Boundaries("tip"))
print("mean tip deflection", u_z/2/h/b)

# alternativ
u_tip = u[2](mesh(l, b/2, 0))
print("point tip deflection" , u_tip)

# %% [markdown]
# ## Weak coupling
# 
# electric field given, compute displacement only

# %%
u_wc = GridFunction(V_u)

U = V_u.TrialFunction()
DELTAU = V_u.TestFunction()

STRAIN = strainvec(U)
DELTASTRAIN = strainvec(DELTAU)
Ezstar = mesh.MaterialCF({"top_mat": 75/h, "bot_mat": -75/h})
Estar = Ezstar*CoefficientFunction((0,0,1))

a = BilinearForm(V_u)
a += SymbolicBFI( InnerProduct(CEtensor*STRAIN - etensor.trans*Estar, DELTASTRAIN) )


# %%
## assemble stiffness matrix and load vector

u_wc.vec[:] = 0
rhs = u_wc.vec.CreateVector()

SetNumThreads(6)
with TaskManager():
    ## evaluate integrals for q=0, i.e. STRAIN=0 and PHITILDE=0 -> negative right hand side of equation
    a.Apply(u_wc.vec, rhs)

    ## compute the stiffness matrix
    a.AssembleLinearization(u_wc.vec)

    ## compute its inverse, for all those degrees of freedom that are not "dirichlet"
    ## use either "pardiso" (MKL sparse solver, usually available on windows/intel chips
    ## or "umfpack" (SuiteSparse solver, usually available on macos/non-intel chips
    inva = a.mat.Inverse(V.FreeDofs(), inverse="pardiso")  # inverse="umfpack"

    ## compute the solution (recall negative rhs)
    u_wc.vec.data -= inva * rhs


## compute average tip deflection 
u_z = Integrate(u_wc[2], mesh, definedon=mesh.Boundaries("tip"))
print("mean tip deflection", u_z/2/h/b)

# alternativ
u_tip = u_wc[2](mesh(l, b/2, 0))
print("point tip deflection" , u_tip)

# %%



