#%%

import netgen.gui

from ngsolve import *
from ngsolve.meshes import MakeStructured3DMesh

import numpy as np

# %%


## structured mesh consists of 1 element
mesh = MakeStructured3DMesh(hexes=True, nx=1, ny=1, nz=1)

Draw(mesh)

# %%

## material parameters

# scale = 1e0 -> m
# scale = 1e3 -> mm
# scale = 1e6 -> um
scale = 1e6

epsilon0 = 8.8541878e-12

CE_matrix = 1e9/scale/scale*np.array([[121, 75.9, 75.4, 0, 0, 0],
                          [75.9, 121, 75.4, 0, 0, 0],
                          [75.4, 75.4, 111, 0, 0, 0],
                          [0, 0, 0, 21.1, 0, 0],
                          [0, 0, 0, 0, 21.1, 0],
                          [0, 0, 0, 0, 0, 22.6]])

e15 = 12.3
e31 = -5.4
e33 = 15.8
e_matrix = 1/scale*np.array([[0, 0, 0, 0, e15, 0],
                     [0, 0, 0, e15, 0, 0],
                     [e31, e31, e33, 0, 0,0]])

epseps_matrix = epsilon0*np.array([[916, 0, 0],[0,916,0],[0,0,830]])
#%%

## material parameters as ngsolve coefficient functions

CEtensor = CoefficientFunction(tuple(CE_matrix.reshape(1, -1)[0]), dims=(6,6))
etensor = CoefficientFunction(tuple(e_matrix.reshape(1, -1)[0]), dims=(3,6))
epstensor = CoefficientFunction(tuple(epseps_matrix.reshape(1, -1)[0]), dims=(3,3))


#%%

## useful - get strain vector representation

def strainvec(u):
    g = grad(u)
    return CoefficientFunction((g[0], g[4], g[8], g[5]+g[7], g[2]+g[6], g[1]+g[3]))

#%%

## the finite element solution space

## for the displacements - vector-valued H1 elements, dirichlet-zero conditions on three surfaces
V_u = VectorH1(mesh, order=1)

## for the electric potential - H1 elements, dirichlet-zero condition on electrode and ground
V_phi = H1(mesh, order=1)

## together
V = V_u * V_phi

#%%

## the solution vector q contains u and phi consecutively

q = GridFunction(V)



#%%

## specify the equations - symbolically, enter principle of virtual work

## symbolic variables, name differently from solution components above!

U, PHI = V.TrialFunction()
DELTAU, DELTAPHI = V.TestFunction()

STRAIN = strainvec(U)
DELTASTRAIN = strainvec(DELTAU)

a = BilinearForm(V, elmatev=True)
a += SymbolicBFI( InnerProduct(CEtensor*STRAIN + etensor.trans*(grad(PHI)), DELTASTRAIN) + InnerProduct(etensor*STRAIN - epstensor*(grad(PHI)), grad(DELTAPHI)) )

#%%

## assemble stiffness matrix and compute element matrix eigenvalues

q.vec[:] = 0

SetTestoutFile("output_scaling_eigenvalues.txt")
## compute the stiffness matrix
a.AssembleLinearization(q.vec)


#%%


