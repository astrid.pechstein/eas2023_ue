#%%

import netgen.gui

from ngsolve import *
from ngsolve.meshes import MakeStructured3DMesh

import numpy as np

# %%

length = 0.1
height = 0.02
width = 0.05

## mapping defines geometry of cantilever:  [0,l] x [-w/2,w/2] x [-h/2,h/2]
def mapping(x,y,z): return (length*x, width*(y-0.5), height*(z-0.5))

## structured mesh 
mesh = MakeStructured3DMesh(hexes=True, nx=20, ny=10, nz=4, mapping=mapping)

Draw(mesh)

#%%

## names for the 6 surfaces
print(mesh.GetBoundaries())

# 'back' .. x=0
# 'left' .. y=-width/2
# 'front' .. x=length
# 'right' .. y=width/2
# 'bottom' .. z=-height/2 
# 'top' .. z=height/2 
# %%

## material parameters

## stiffness, given e.g. as a numpy array

CE_matrix = 1e9*np.array([[121, 75.9, 75.4, 0, 0, 0],
                          [75.9, 121, 75.4, 0, 0, 0],
                          [75.4, 75.4, 111, 0, 0, 0],
                          [0, 0, 0, 21.1, 0, 0],
                          [0, 0, 0, 0, 21.1, 0],
                          [0, 0, 0, 0, 0, 22.6]])


## density

rho = 7750

#%%

## material parameters as ngsolve coefficient functions -- convert np.array to tuple first
## set dimensions 6x6 matrix

CEtensor = CoefficientFunction(tuple(CE_matrix.reshape(1, -1)[0]), dims=(6,6))


#%%

## useful - get strain vector representation from displacement vector

def strainvec(u):
    g = grad(u)
    return CoefficientFunction((g[0], g[4], g[8], g[5]+g[7], g[2]+g[6], g[1]+g[3]))

#%%

## the finite element solution space

## for the displacements - vector-valued H1 elements, dirichlet-zero conditions on "back"
V = VectorH1(mesh, order=1, dirichlet="back")


#%%

## GridFunction for the solution u
## the solution vector can be accessed by u.vec

u = GridFunction(V)

## plot displacement function
Draw(u, mesh, "u")

#%%

## access solutions dof vector

#print(u.vec)
#u.vec[3] = 1

#%%

## stress and strain
strain = strainvec(u)
stress = CEtensor*strain

## plot these fields
Draw(BoundaryFromVolumeCF(strain), mesh, "strain")
Draw(BoundaryFromVolumeCF(stress), mesh, "stress")



#%%

## specify the equations - symbolically, enter principle of virtual work

## symbolic variables, name differently from solution above!

U = V.TrialFunction()
DELTAU = V.TestFunction()

STRAIN = strainvec(U)
DELTASTRAIN = strainvec(DELTAU)

a = BilinearForm(V)
a += SymbolicBFI( InnerProduct(CEtensor*STRAIN, DELTASTRAIN) )
a += SymbolicBFI( 9.81*rho* DELTAU[2])

## tip load
# t = CoefficientFunction((0, 1e8, 1e4))
# a += SymbolicBFI( -InnerProduct(t, DELTAU), definedon=mesh.Boundaries("front"))
#%%

## assemble stiffness matrix and load vector

u.vec[:] = 0
rhs = u.vec.CreateVector()

## evaluate integrals for u=0, i.e. STRAIN=0 -> negative right hand side of equation
a.Apply(u.vec, rhs)

## compute the stiffness matrix (linearized at u)
a.AssembleLinearization(u.vec)

## compute its inverse, for all those degrees of freedom that are not "dirichlet"
## use either "pardiso" (MKL sparse solver, usually available on windows/intel chips
## or "umfpack" (SuiteSparse solver, usually available on macos/non-intel chips
inva = a.mat.Inverse(V.FreeDofs(), inverse="pardiso")  # inverse="umfpack"

## compute the solution (recall negative rhs)
u.vec.data -= inva * rhs



#%%


