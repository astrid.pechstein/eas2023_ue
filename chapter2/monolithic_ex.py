#%%

import netgen.gui

from ngsolve import *
from ngsolve.meshes import MakeStructured3DMesh

import numpy as np

# %%

length = 0.05
height = 0.001
width = 0.05

## mapping defines 1/8 of the original monolithic plate [-l/2,l/2] x [-w/2,w/2] x [-h/2,h/2]
def mapping(x,y,z): return (height*(x-0.5), width*(y-0.5), length*(z-0.5))

## structured mesh consists of 1 element
mesh = MakeStructured3DMesh(hexes=True, nx=1, ny=1, nz=1, mapping=mapping)

Draw(mesh)

#%%

## names for the 6 surfaces
print(mesh.GetBoundaries())

# 'back' .. x=-h/2 .. Gamma^u
# 'left' .. y=-width/2
# 'front' .. x=height/2.. Gamma^o
# 'right' .. y=width/2
# 'bottom' .. z=-length/2 
# 'top' .. z=length/2 
# %%

## material parameters
epsilon0 = 8.8541878e-12

CE_matrix = 1e9*np.array([[121, 75.9, 75.4, 0, 0, 0],
                          [75.9, 121, 75.4, 0, 0, 0],
                          [75.4, 75.4, 111, 0, 0, 0],
                          [0, 0, 0, 21.1, 0, 0],
                          [0, 0, 0, 0, 21.1, 0],
                          [0, 0, 0, 0, 0, 22.6]])

e15 = 12.3
e31 = -5.4
e33 = 15.8
e_matrix = np.array([[0, 0, 0, 0, e15, 0],
                     [0, 0, 0, e15, 0, 0],
                     [e31, e31, e33, 0, 0,0]])

epseps_matrix = epsilon0*np.array([[916, 0, 0],[0,916,0],[0,0,830]])
#%%

## material parameters as ngsolve coefficient functions

CEtensor = CoefficientFunction(tuple(CE_matrix.reshape(1, -1)[0]), dims=(6,6))
etensor = CoefficientFunction(tuple(e_matrix.reshape(1, -1)[0]), dims=(3,6))
epstensor = CoefficientFunction(tuple(epseps_matrix.reshape(1, -1)[0]), dims=(3,3))


#%%

## useful - get strain vector representation

def strainvec(u):
    g = grad(u)
    return CoefficientFunction((g[0], g[4], g[8], g[5]+g[7], g[2]+g[6], g[1]+g[3]))

#%%

## the finite element solution space

## for the displacements - vector-valued H1 elements, dirichlet-zero conditions on three surfaces
V_u = VectorH1(mesh, order=1, dirichlet="back")

## for the electric potential - H1 elements, dirichlet-zero condition on electrode and ground
V_phi = H1(mesh, order=1, dirichlet="back|front")

## together 
V = V_u * V_phi

#%%

## the solution vector q contains u and phi consecutively

q = GridFunction(V)

u = q.components[0]
phi = q.components[1]

strain = strainvec(u)
elfield = -grad(phi)

stress = CEtensor*strain - etensor.trans*elfield
diel = etensor*strain + epstensor*elfield

Draw(u, mesh, "u")
Draw(phi, mesh, "phi")
Draw(BoundaryFromVolumeCF(elfield), mesh, "E")
Draw(BoundaryFromVolumeCF(strain), mesh, "strain")
Draw(BoundaryFromVolumeCF(diel), mesh, "D")
Draw(BoundaryFromVolumeCF(stress), mesh, "stress")


#%%

## boundary conditon for electric potential - define the potential phi_0 as finite element function
phi_0 = GridFunction(V_phi)

phi_bdvalues = mesh.BoundaryCF({"back": 50, "front": -50}, default=0)
phi_0.Set(phi_bdvalues, definedon=mesh.Boundaries("back|front"))

#%%

## specify the equations - symbolically, enter principle of virtual work

## symbolic variables, name differently from solution components above!

U, PHITILDE = V.TrialFunction()
DELTAU, DELTAPHI = V.TestFunction()

STRAIN = strainvec(U)
DELTASTRAIN = strainvec(DELTAU)

a = BilinearForm(V)
a += SymbolicBFI( InnerProduct(CEtensor*STRAIN + etensor.trans*(grad(PHITILDE)+grad(phi_0)), DELTASTRAIN) )
a += SymbolicBFI( InnerProduct(etensor*STRAIN - epstensor*(grad(PHITILDE)+grad(phi_0)), grad(DELTAPHI)) )

#%%

## assemble stiffness matrix and load vector

q.vec[:] = 0
rhs = q.vec.CreateVector()

## evaluate integrals for q=0, i.e. STRAIN=0 and PHITILDE=0 -> negative right hand side of equation
a.Apply(q.vec, rhs)

## compute the stiffness matrix
a.AssembleLinearization(q.vec)

## compute its inverse, for all those degrees of freedom that are not "dirichlet"
## use either "pardiso" (MKL sparse solver, usually available on windows/intel chips
## or "umfpack" (SuiteSparse solver, usually available on macos/non-intel chips
inva = a.mat.Inverse(V.FreeDofs(), inverse="pardiso")  # inverse="umfpack"

## compute the solution (recall negative rhs)
q.vec.data -= inva * rhs

## add phi_0 to electric potential
phi.vec.data += phi_0.vec

#%%


